#!/bin/sh -e

VERSION=$2
TAR=../cssparser_$VERSION.orig.tar.gz
DIR=cssparser-$VERSION
TAG=$(echo "CSSPARSER_$VERSION" | sed -re 's/\./_/g')


cvs -z3 -d:pserver:anonymous@cssparser.cvs.sourceforge.net:/cvsroot/cssparser -Q export -r $TAG -d $DIR cssparser

GZIP=--best tar -c -z -f $TAR --exclude '*.jar' --exclude '*.class' --exclude '.cvsignore' $DIR
rm -rf $DIR ../$TAG

